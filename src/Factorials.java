public class Factorials {
    static int calculateFactorial(int n) {
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        return result;
    }

    static int calculateSumOfFactorials(int firstNumber, int secondNumber) {
        int start = (firstNumber % 2 == 1) ? firstNumber : firstNumber + 1;
        int end = (secondNumber % 2 == 1) ? secondNumber : secondNumber - 1;
        int result = 0;
        for (int i = start; i <= end; i += 2) {
            result += calculateFactorial(i);
        }
        return result;
    }
}
