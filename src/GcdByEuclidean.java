public class GcdByEuclidean {

    public static Integer getGcdByEuclidean(int a, int b) {
        if (a == 0 && b == 0) {
            throw new IllegalArgumentException("Two numbers cannot be 0 at the same time.");
        }

        if (a == Integer.MAX_VALUE || b == Integer.MIN_VALUE) {
            throw new IllegalArgumentException("Number cannot be {int.MinValue}.");
        }

        while (b != 0) {
            {
                int remainder = a % b;
                a = b;
                b = remainder;
            }
        }

        return Math.abs(a);
    }

    public static int getGcdByEuclidean(int a, int b, int... other) {
        boolean full_zero_flag = true;
        int not_zero_element_pos = 0;
        for (int i = 0; i < other.length; i++) {
            if (other[i] == Integer.MIN_VALUE) {
                throw new IllegalArgumentException("Number cannot be {int.MinValue}.");
            }

            Math.abs(other[i]);
            if (other[i] != 0) {
                full_zero_flag = false;
                not_zero_element_pos = i;
            }
        }

        if (full_zero_flag && a == 0 && b == 0) {
            throw new IllegalArgumentException("All numbers cannot be 0 at the same time.");
        }

        int cup;
        if (a == 0 && b == 0) {
            cup = getGcdByEuclidean(a, other[not_zero_element_pos]);
        } else {
            cup = getGcdByEuclidean(a, b);
        }

        cup = getGcdByEuclidean(cup, b);
        cup = getGcdByEuclidean(cup, a);
        for (int j : other) {
            cup = getGcdByEuclidean(cup, j);
        }

        return cup;
    }
}
