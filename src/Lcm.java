public class Lcm {
    public static Integer getLcm(int a, int b) {
        return Math.abs(a * b) / GcdByEuclidean.getGcdByEuclidean(a, b);
    }

    public static long getLcm(int a, int b, int... other) {
        boolean full_zero_flag = true;
        int not_zero_element_pos = 0;
        for (int i = 0; i < other.length; i++) {
            if (other[i] == Integer.MIN_VALUE) {
                throw new IllegalArgumentException("Number cannot be {int.MinValue}.");
            }

            Math.abs(other[i]);
            if (other[i] != 0) {
                full_zero_flag = false;
                not_zero_element_pos = i;
            }
        }

        if (full_zero_flag && a == 0 && b == 0) {
            throw new IllegalArgumentException("All numbers cannot be 0 at the same time.");
        }

        int cup;
        if (a == 0 && b == 0) {
            cup = getLcm(a, other[not_zero_element_pos]);
        } else {
            cup = getLcm(a, b);
        }

        cup = getLcm(cup, b);
        cup = getLcm(cup, a);
        for (int j : other) {
            cup = getLcm(cup, j);
        }

        return cup;
    }
}
