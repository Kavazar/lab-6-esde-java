public class SecondLargest {
    public static int getLargest(int[] a) {
        int temp = a[0];
        for (int i = 1; i < a.length; i++) {
            if (temp < a[i]) {
                temp = a[i];
            }
        }
        return temp;
    }

    public static int getSecondLargest(int[] a) {
        int largestNumber = getLargest(a);
        int temp = a[0];
        for (int i = 1; i < a.length; i++) {
            if (temp < a[i] && a[i] < largestNumber) {
                temp = a[i];
            }
        }
        return temp;
    }
}
