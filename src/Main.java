public class Main {
    public static void main(String[] args) {
        System.out.println(GcdByEuclidean.getGcdByEuclidean(6, 18, 9, 36, 4));
        System.out.println(GcdByEuclidean.getGcdByEuclidean(6, 18));
        System.out.println();

        System.out.println(Lcm.getLcm(25, 28, 50));
        System.out.println(Lcm.getLcm(25, 28));
        System.out.println();

        System.out.println(Factorials.calculateFactorial(5));
        System.out.println(Factorials.calculateSumOfFactorials(1, 9));
        System.out.println();
        
        System.out.println(SecondLargest.getLargest(new int[]{1, 4, 2, 3}));
        System.out.println(SecondLargest.getSecondLargest(new int[]{1, 4, 2, 3}));
    }
}

